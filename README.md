# [**step-project-forkio**](https://marcik357.gitlab.io/step-project-forkio/) - це проект для вдосконалення навичок роботи з git в команді, з адаптивною версткою та препроцесорами SASS SCSS



## При роботі над проектом використовувались такі технології:

* Методолгія найменування класів BEM;
* Принцип Mobile first;
* HTML з використанням семантичних тегів;
* CSS препроцесор SASS SCSS:
    * _Variables_
    * _Mixins_
    * _Functions_
    * _Сепарація та імпортування файлів_
* Adaptive and responsive design;
* NPM модулі:
    * _del_
    * _gulp-autoprefixer_
    * _gulp-clean-css_
    * _gulp-file-include_
    * _gulp-group-css-media-queries_
    * _gulp-if_
    * _gulp-imagemin_
    * _gulp-newer_
    * _gulp-notify_
    * _gulp-plumber_
    * _gulp-rename_
    * _gulp-replace_
    * _gulp-sass_
    * _gulp-version-number_
    * _gulp-webp_
    * _gulp-webp-html-nosvg_
    * _gulp-webpcss_
    * _sass_
    * _webp-converter_
    * _webpack_
    * _webpack-stream_
* Проект був зібраний на основі Gulp;
---

## Склад розробників проекту:
* ___Михайліченко Антон___
* ___Лілія Чех___

---

## Список виконаних робіт:
* ___Михайліченко Антон___:
    * Створення збірки gulp
    * Зверстав та реалізував функціонал `header` сайту
    ![header](/readme_assets/header.jpg)
    ![header_mobile](/readme_assets/header_mobile.jpg)
    * Зверстав `"перший екран"`
    ![first_screen](/readme_assets/first_screen.jpg)
    * Зверстав секцію `People Are Talking About Fork`
    ![testimonials](/readme_assets/testimonials.jpg)

* ___Лілія Чех___: 
    * Зверстала секцію `Present text`
   ![present-text](/readme_assets/present-text.jpg)
    * Зверстала секцію `Features`
   ![features](/readme_assets/features.jpg)
    * Зверстала секцію `Pricing`
   ![pricing](/readme_assets/pricing.jpg)

---   

## Основні команди:
    1. npm i - ініциалізація проекту
    2. npm run build - зробити готовий до продакшену білд
    3. npm run dev - запуск в режимі розробки

