// Need to create webp images
export function isWebp() {
    // CanIuse webp 
    function testWebP(callback) {
        let webP = new Image();
        webP.onload = webP.onerror = function () {
            callback(webP.height == 2);
        };
        webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
    }
    // Add class _webp or _no-webp to HTML
    testWebP(function (support) {
        let className = support === true ? 'webp' : 'no-webp';
        document.documentElement.classList.add(className);
    });
}

// Open/Close mobile menu
export function animateMobileMenu() {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav');
    // Toggle showing mobile menu
    document.body.addEventListener('click', toggleNavMenu);
    function toggleNavMenu(e) {
        if (e.target.closest('.burger') || e.target.closest('.nav__link')) {
            burger.classList.toggle('burger--active');
            nav.classList.toggle('nav--open');
        } else if (!e.target.closest('.nav') && nav.classList.contains('nav--open')) {
            e.preventDefault();
            nav.classList.remove('nav--open');
            burger.classList.remove('burger--active');
        }
    }
}

// Change header on scroll
export function changeHeader() {
    window.addEventListener('scroll', headerChange);
    function headerChange() {
        let header = document.querySelector('.header')
        if (window.scrollY >= 30) {
            header.classList.add('header--on-scroll')
        } else if (window.scrollY === 0) {
            header.classList.remove('header--on-scroll')
        }
    };
}